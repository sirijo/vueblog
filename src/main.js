import Vue from 'vue';
import App from './App.vue';

import './../node_modules/bulma/css/bulma.css';
import './../node_modules/bulma-timeline/dist/css/bulma-timeline.min.css';
import './../node_modules/bulma-divider/dist/css/bulma-divider.min.css';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faCoffee, faHome, faMailBulk, faEnvelope, faCopyright } from '@fortawesome/free-solid-svg-icons';
import { faLinkedinIn, faGithub, faGitlab } from  '@fortawesome/free-brands-svg-icons';

import { FontAwesomeIcon, FontAwesomeLayers, FontAwesomeLayersText } from '@fortawesome/vue-fontawesome'

library.add(faCoffee, faHome, faMailBulk, faEnvelope, faLinkedinIn, faGithub, faGitlab, faCopyright);

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component('font-awesome-layers', FontAwesomeLayers);
Vue.component('font-awesome-layers-text', FontAwesomeLayersText);


Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
